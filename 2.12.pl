dropa([],_,NewL,_).
    % write(NewL)
dropa([H|T],N,NewL,Count):-
    Count1 is Count+1,
    (Count1=N
     -> (dropa(T,N,NewL,0))
     ;  (append(NewL,[H],Final), dropa(T,N,Final,Count1))).


drop(List,N,NewL):-
    dropa(List,N,NewL,0).