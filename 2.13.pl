
remove([],NewL):-
   write(NewL).

remove([H|T],NewL):-
    (member(H,NewL)
    ->remove(T,NewL)
    ;append(NewL,[H],NewL1), remove(T,NewL1)).

remove_duplicate(List):-
    remove(List,[]).
    