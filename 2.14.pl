maplist_sol(L1,L2,L3):-
    maplist(sum,L1,L2,L3).

sum(L1, L2, L3) :-
    L3 is L1 + L2.