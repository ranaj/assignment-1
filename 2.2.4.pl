sum([],0).
sum([H|T],Ans):-
    (is_list(H)
      -> sum(H,Ans1),sum(T,Ans2),
         Ans is Ans1+Ans2
    ; sum(T,Ans3) , Ans is Ans3+H).
