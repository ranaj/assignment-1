% ques 5
find(X,[]):-
    write("Not present").

find(X,[X|T]):-
    write("Present").            

find(X,[Y|T]):-
    find(X,T).