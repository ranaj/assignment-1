% ques 7
duplicate_elements([H|[]],X,Y):- %Base Case
    append(X,[H,H],Y).

duplicate_elements([H|T],List,Final_Ans):- %Body
    duplicate_elements(T,List,Ans),
    append([H,H],Ans,Final_Ans).