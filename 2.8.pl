% ques 8
   duplicate_N(L1,2,L2):-
    append(L1,L1,L2).

   duplicate_N(L1,N,L2):-
    N1 is N-1,
   duplicate_N(L1,N1,L3),
   append(L3,L1,L2).