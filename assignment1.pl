                                                %Assignment 1                                           
 % ques 1
insert_db1(X):-
    assert(X).

insert_db2(X):-
    assert(X).

remove_db1(X):-
    retract(X).

remove_all(X):-
    retractall(X).

% ques 2
remove_db1_with_element_a(X):-
    retract(db1(X)).

% ques 3
insert_func(X):-
    assert(db1(func(X))).
remove(X):-
    retractall(db1(func(a))).

% ques 4
% retract(_) removes the first element of the database whereas retractall(_) removes all element of database

% ques 5
diff(X,Y):-
    X\=Y.
% Answer- All three diff(X,Y), diff(X,_), diff(X,a) are false as value of a variable is unkown
% but diff(a,b) is false.

% ques 6
% 1
max1(W,X,Y,Z):-
    W>=X, W>=Y ,W>=Z
      -> (write(W));
    
    X>=W , X>=Y , X>=Z
      -> (write(X));

    Y>=X , Y>=W , Y>=Z
     -> (write(Y));

     Z>=X , Z>=Y , Z>=W
       -> (write(Z)).

    
       
max2(W,X,Y,Z):-
   W>=X
     -> (W>=Y
         -> (W>=Z
             -> (write(W))
             ; write(Z))
      ; Y>=Z   
            -> (write(Y) )
            ; write(Z))
    ; X>=Y
        -> (X>=Z
            -> write(X)
            ; write(Z))
    ;  (Y>=Z)
        -> (write(Y))
        ; write(Z)   . 

          

     
    



    



